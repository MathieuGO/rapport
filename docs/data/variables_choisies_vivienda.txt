variable,explication
ID_VIV,identifiant de l'habitation
ENT,code Entidad Federativa
NOM_ENT, nom Entidad Federativa
MUN, code Municipio
NOM_MUN, Nom Municipio
code_mun, code ENT + MUN 
LOC50K, code localidad
NOM_LOC, code localidad
FACTOR,facteur de pondération de la vivienda
CLAVIVP,type logement
PISOS,type de sols
TOTCUART,nombre de pièces
CUADORM,nombre de chambres
COMBUSTIBLE,type de chauffage
ELECTRICIDAD,Electricité
AGUA_ENTUBADA,Arrivée d'eau
ABA_AGUA_ENTU,approvisionnement en eau
ABA_AGUA_NO_ENTU,approvisionnement en eau
AIRE_ACON,air conditionné
SERSAN,toilettes
USOEXC,toilettes partagés
DRENAJE,evacuation des eaux usées
DESTINO_BASURA,traitement des ordures ménagères
REFRIGERADOR,réfrigérateur
LAVADORA,lavabo
HORNO,four
AUTOPROP,voiture
RADIO,récepteur radio
TELEVISOR,télévision
TELEVISOR_PP,télévision à écran plat
COMPUTADORA,ordinateur
TELEFONO,ligne de téléphone terrestre
CELULAR, téléphone mobile
INTERNET,accès internet
TENENCIA,propriétaire du logement
NUMPERS,nombre de résidents du logement
TIPOHOG,type de foyer
