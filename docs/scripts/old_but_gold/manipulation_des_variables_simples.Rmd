---
title: "manipulation des variables 2015"
author: "Nicolas"
date: "20 novembre 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
```{r chargement des bibliothèques}
library(readr)
library(dplyr)
library(data.table)
library(questionr)
```
```{r cars}

persona_vivienda_zm_mexico_2015 <- read_csv("data/persona_vivienda_zm_mexico_2015.csv", 
    col_types = cols(COBERTURA_x = col_character(), 
        ID_PERSONA = col_character(), ID_VIV = col_character(), 
        LOC50K_x = col_character(), X1 = col_skip()))
#View(persona_vivienda_zm_mexico_2015)
```

# simplification des données:
## filtre par les variables  sélectionnées

```{r}
colnames(persona_vivienda_zm_mexico_2015)
length(colnames(persona_vivienda_zm_mexico_2015))
colnames(persona_vivienda_zm_mexico_2015)[order(colnames(persona_vivienda_zm_mexico_2015))]
```

Il y a 175 variables dans le fichier concaténant les persona et vivienda de la zone d'études.

```{r}
variables_choisies <- read_csv("data/variables_choisies.txt")
variables_choisies[order(variables_choisies$variable), ]
dim(variables_choisies)
```
Nous avons choisi de limiter notre étude à seulement 44 variables (dont certaines seront agrégées dans des variables plus synthétiques)
```{r}
persona_vivienda_zm_mexico_2015_concentration <-persona_vivienda_zm_mexico_2015[,variables_choisies$variable]
dim(persona_vivienda_zm_mexico_2015_concentration)
```
Le jeu de données centré sur la **ZUM** comprend 1 645 437 enregistrements (*Persona*), contre plus de 22 Million au départ et 44 variables des 175 variables disponibles initialement.


## Renommage d'un certain nombre de colonnes

```{r}
old_names <- c("ENT_x",
"NOM_ENT_x",
"MUN_x",
"NOM_MUN_x",
"LOC50K_x",
"NOM_LOC_x",
"FACTOR_x",
"FACTOR_y")

new_names <- c("ENT",
"NOM_ENT",
"MUN",
"NOM_MUN",
"LOC50K",
"NOM_LOC",
"FACTOR_PERS",
"FACTOR_VIV")

changement_noms <- data.frame(old_names,new_names)
changement_noms 


```

```{r}

setnames(persona_vivienda_zm_mexico_2015_concentration, old=c("ENT_x",
"NOM_ENT_x",
"MUN_x",
"NOM_MUN_x",
"LOC50K_x",
"NOM_LOC_x",
"FACTOR_x",
"FACTOR_y"), new=c("ENT",
"NOM_ENT",
"MUN",
"NOM_MUN",
"LOC50K",
"NOM_LOC",
"FACTOR_PERS",
"FACTOR_VIV"))
colnames(persona_vivienda_zm_mexico_2015_concentration)[order(colnames(persona_vivienda_zm_mexico_2015_concentration))]
```
```{r}
head(persona_vivienda_zm_mexico_2015_concentration)
```

```{r}
dim(persona_vivienda_zm_mexico_2015_concentration)
```



## Création de codes supplémentaires
### Code MUN
```{r}
persona_vivienda_zm_mexico_2015_concentration$CODE_MUN <- paste(persona_vivienda_zm_mexico_2015_concentration$ENT, persona_vivienda_zm_mexico_2015_concentration$MUN, sep='') 
length(unique(persona_vivienda_zm_mexico_2015_concentration$CODE_MUN))
```

Il y a bien 76 *Municipios* dans le jeu de données.

### Code LOC50K
```{r}
persona_vivienda_zm_mexico_2015_concentration$CODE_LOC <- paste(persona_vivienda_zm_mexico_2015_concentration$CODE_MUN, persona_vivienda_zm_mexico_2015_concentration$LOC50K, sep='') 
length(unique(persona_vivienda_zm_mexico_2015_concentration$CODE_LOC))
```
Il apparait que notre zone d'étude comprend 107 *Localidad*.


## Export en CSV
```{r}
write.csv(persona_vivienda_zm_mexico_2015_concentration, file = "data/traitees/persona_vivienda_zm_mexico_2015_concentration.csv",row.names=FALSE, na="")
```
## Agrégation par code LOC50K

```{r}
head(persona_vivienda_zm_mexico_2015_concentration)
```

Agrégation par MUN LOC sur plusieurs variables:
tri croisé avec pondération (p21)
```{r}
# persona_vivienda_zm_mexico_2015_concentration
classe_habitat_LOC <- wtd.table(persona_vivienda_zm_mexico_2015_concentration$CODE_LOC, persona_vivienda_zm_mexico_2015_concentration$CLAVIVP, weights = persona_vivienda_zm_mexico_2015_concentration$FACTOR_VIV)
dim(classe_habitat_LOC)
classe_habitat_LOC
```
# Taux d'occupation des logements 

2010: variable "nombre d'occupants par pièce"

## Indice de ségrégation Duncan Duncan 
http://cybergeo.revues.org/12063
https://oasis.irstea.fr/wp-content/uploads/2013/10/03-Egalit%c3%a9.pdf

## Indice de dissimilarité
http://geoconfluences.ens-lyon.fr/glossaire/indices-de-dissimilarite

Mesurer l'écart entre les deux dates ou entre 2 villes.

# Spécialisation activité économique
Indice d'Isard (https://en.wikipedia.org/wiki/Walter_Isard):
1/2 * somme(Mij/Mi - Mj/M..)

Mij: employé dans cette branche dans ce quartier
Mi: employés dans ce quartier
Mj: employé de la branche étudiée dans la ville
M..: total des employés de la ville

# Exemple de calcul statistique: part des moins de 15 ans.

```{r}
persona_vivienda_zm_mexico_2010 <- read.csv("~/Rstuff/M2_projet_transversal/docs/data/persona_vivienda_zm_mexico_2010.csv", encoding="UTF8", na.strings="*")
```

```{r}
summary(persona_vivienda_zm_mexico_2010$VPH_INTER)

```
