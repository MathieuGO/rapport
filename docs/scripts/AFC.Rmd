---
title: "AFC"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r }
library("FactoMineR")
library("factoextra")
library("corrplot")
library('dplyr')
```

```{r }
# Chargement des données
donnees <- read.csv("../data/traitees/tab_frequence_2015_viv.csv", encoding="UTF-8", head=T)
donnees_persona <- read.csv("../data/traitees/tab_frequence_2015_pers.csv", encoding="UTF-8", head=T)
#colnames(donnees)
#colnames(donnees_persona)

```

```{r Exemple, une variable}
####Exemple sur 1 variable
#Choix de variable CLAVIVP


```

```{r }
### Les boucles pour AFC 
variables_choisies <- read.csv("../data/variables_choisies_vivienda.txt")
variables <- variables_choisies[10:37,]$variable
variables <- as.character(variables)
code_com <- variables_choisies[6,]$variable


resAFC <- list()
for (i in 1:28){
   #print(variables[i])
  
  #select des variables pour AFC
  a<- select(donnees, starts_with(variables[i]))
  #ajouter code_mun
  row.names(a)<- donnees$code_mun
  #AFC
  res.ca <- CA(a, graph = FALSE)
  #Stockage de résultats d'AFC pour toutes les variables dans une liste
  tmp <- res.ca
  resAFC[[variables[i]]] <- tmp
 
}



```


```{r AFC Persona}
variables_choisies_persona <- read.csv("../data/variables_choisies_persona.txt")
variables_pers <- variables_choisies_persona[10:15,]$variable
variables_pers <- as.character(variables_pers)


resAFC_pers <- list()
for (i in 1:6){
   #print(variables[i])
  
  #select des variables pour AFC
  a<- select(donnees_persona, starts_with(variables_pers[i]))
  #ajouter code_mun
  row.names(a)<- donnees_persona$code_mun
  #AFC
  res.ca <- CA(a, graph = FALSE)
  #Stockage de résultats d'AFC pour toutes les variables dans une liste
  tmp <- res.ca
  resAFC_pers[[variables_pers[i]]] <- tmp
 
}


```

```{r test sur exemple}
#Pour accéder aux résultats de chaque AFC :resAFC$CLAVIVP
#resAFC$CLAVIVP
clavivp <-resAFC$CLAVIVP

#visualisation des valeurs propres
#fviz_screeplot (resAFC$CLAVIVP, addlabels = TRUE, ylim = c(0, 70))
#fviz_ca_biplot (resAFC$CLAVIVP, label="col",  alpha.row=0.3, col.row = "#00AFBB", col.col = "#0e07e5", repel = TRUE)


```

```{r Valeurs prpres VIVIENDA}
#visualisation des valeurs propres pour chaque variable
for (var in variables) {
  vartemp <- resAFC[[var]]
  final.plot <-fviz_screeplot (vartemp, addlabels = TRUE, ylim = c(0, 100))
  print(final.plot + labs(title = paste("Valeurs propres", var, sep = " "),
         x = "Facteur", y = "% de variance"))
  #creation de nom unique pour chaque image et sauvegarde en jpeg
  nam <- paste("../images/AFC/valeurs_propres_viv_", var, ".jpeg", sep="")
  #nam1<-  paste("![Variances expliquées de la variable ", var, "](../../static/img/AFC/valeurs_propres_viv_", var, ".jpeg", sep="")
  
   
  assign(nam, var)
  #  assign(nam1, var)
  jpeg(filename = nam)
  plot(final.plot)
  dev.off()
  #print(nam1)
  
}


 
```

```{r Valeurs propres Persona}
#visualisation des valeurs propres pour chaque variable
for (var in variables_pers) {
  vartemp <- resAFC_pers[[var]]
  final.plot <-fviz_screeplot (vartemp, addlabels = TRUE, ylim = c(0, 100))
  print(final.plot + labs(title = paste("Valeurs propres", var, sep = " "),
         x = "Facteur", y = "% de variance"))
  nam <- paste("../images/AFC/valeurs_propres_pers_", var, ".jpeg", sep="")
  nam1 <- paste("![Variances expliquées de la variable ", var, "](../../static/img/AFC/valeurs_propres_viv_", var, ".jpeg", sep="")
  assign(nam, var)
  assign(nam1,var)
  jpeg(filename = nam)
  plot(final.plot)
  dev.off()
  print(nam1)
}


```



```{r Biplots VIVIENDA}
#creation des biplots pour totes les variables
for (var in variables) {
  vartemp <- resAFC[[var]]
  final.plot <-fviz_ca_biplot (vartemp, label="col",  alpha.row=0.5, col.row = "#00AFBB", col.col = "#0e07e5", repel = TRUE)
  print(final.plot)
  nam <- paste("../images/AFC/biblot_viv_", var, ".jpeg", sep="")
  assign(nam, var)
  jpeg(filename = nam)
  plot(final.plot)
  dev.off()
  
}
```
```{r Biplots PERSONA}
#creation des biplots pour totes les variables
for (var in variables_pers) {
  vartemp <- resAFC_pers[[var]]
  final.plot <-fviz_ca_biplot (vartemp, label="col",  alpha.row=0.5, col.row = "#00AFBB", col.col = "#0e07e5", repel = TRUE)
  print(final.plot)
  nam <- paste("../images/AFC/biplot_pers_", var, ".jpeg", sep="")
  assign(nam, var)
  jpeg(filename = nam)
  plot(final.plot)
  dev.off()
  
}
```



```{r CTR VIVIENDA}
#faire le graphique de contributions sur le premiees 5 axes
for (var in variables) {
  vartemp <- resAFC[[var]]
  col <-  get_ca_col(vartemp)
  #final.plot <-corrplot(col$contrib, is.corr=FALSE, title = "Contributions des fréquences actives")
  #print(final.plot)
  nam <- paste("../images/AFC/CTR_viv_", var, ".jpeg", sep="")
  assign(nam, var)
  jpeg(filename = nam)
  corrplot(col$contrib, is.corr=FALSE, title = "Contributions des fréquences actives")
  dev.off()
  
}
```

```{r CTR PERSONA}
#faire le graphique de contributions sur le premiees 5 axes

for (var in variables_pers) {
  vartemp <- resAFC_pers[[var]]
  col <-  get_ca_col(vartemp)
  #final.plot <-corrplot(col$contrib, is.corr=FALSE, title = c("Contributions des fréquences actives", var))
  #print(final.plot)
  nam <- paste("../images/AFC/CTR_pers_", var, ".jpeg", sep="")
  assign(nam, var)
  jpeg(filename = nam)
  corrplot(col$contrib, is.corr=FALSE, title = c("Contributions des fréquences actives", var))
  dev.off()
}

```

```{r Cos2 VIVIENDA}
#faire le graphique de COS2 sur le premiees 5 axes
for (var in variables) {
  vartemp <- resAFC[[var]]
  col <-  get_ca_col(vartemp)
 # final.plot <-corrplot(col$cos2, is.corr=FALSE, title = "Qualités de représentation (COS2) des fréquences actives")
  nam <- paste("../images/AFC/COS_viv_", var, ".jpeg", sep="")
  assign(nam, var)
  jpeg(filename = nam)
  corrplot(col$cos2, is.corr=FALSE, title = "Qualités de représentation (COS2) des fréquences actives")
  dev.off()
}

```

```{r Cos2 PERSONA}
#faire le graphique de COS2 sur le premiees 5 axes
for (var in variables_pers) {
  vartemp <- resAFC_pers[[var]]
  col <-  get_ca_col(vartemp)
 # final.plot <-corrplot(col$cos2, is.corr=FALSE, title = "Qualités de représentation (COS2) des fréquences actives")
  nam <- paste("../images/AFC/COS_pers_", var, ".jpeg", sep="")
   jpeg(filename = nam)
   assign(nam, var)
 
  corrplot(col$cos2, is.corr=FALSE, title = "Qualités de représentation (COS2) des fréquences actives")
  dev.off()
  
}


```



```{r }

```






