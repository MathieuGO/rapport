
# Constitution des bases



```python
# vérifications préliminaires
import sys
sys.version
from os import getcwd
os.getcwd()
```


```python
import glob
import pandas as pd
import numpy as np
```


```python
liste_fichier = []
chemin_fichier = os.getcwd() + '/mexique/CSV/'
print(chemin_fichier)
for fichier in glob.glob('mexique/CSV/*'):
    liste_fichier.append(fichier)

liste_fichier
```

## Séparation des fichiers de la Zone métropolitaine de Mexico


```python
liste_ent = [] # liste de tous les ENT
liste_zm_mexico = [] # Liste des fichiers concernant la ZM de Mexico

counter = 1

for file in liste_fichier:
    fichier = pd.read_csv(file, encoding = 'latin_1')
    ent = fichier.iloc[0]['NOM_ENT']
    print(counter, '. ',file, ': ', ent)
    liste_ent.append(ent)
    counter +=1 
    #if fichier.iloc[0]['NOM_ENT'] in ('México','Distrito Federal'):
        #print(file)
        #liste_zm_mexico.append(file)
    
```

    1 .  mexique/CSV/TR_PERSONA11.CSV :  Guanajuato
    2 .  mexique/CSV/TR_VIVIENDA19.CSV :  Nuevo León
    3 .  mexique/CSV/TR_PERSONA19.CSV :  Nuevo León
    4 .  mexique/CSV/TR_PERSONA02.CSV :  Baja California
    5 .  mexique/CSV/TR_PERSONA05.CSV :  Coahuila de Zaragoza
    6 .  mexique/CSV/TR_VIVIENDA02.CSV :  Baja California
    7 .  mexique/CSV/TR_VIVIENDA23.CSV :  Quintana Roo
    8 .  mexique/CSV/TR_PERSONA21.CSV :  Puebla
    9 .  mexique/CSV/TR_PERSONA27.CSV :  Tabasco
    10 .  mexique/CSV/TR_VIVIENDA08.CSV :  Chihuahua
    11 .  mexique/CSV/TR_PERSONA23.CSV :  Quintana Roo
    12 .  mexique/CSV/TR_VIVIENDA31.CSV :  Yucatán
    13 .  mexique/CSV/TR_VIVIENDA01.CSV :  Aguascalientes
    14 .  mexique/CSV/TR_PERSONA07.CSV :  Chiapas
    15 .  mexique/CSV/TR_VIVIENDA10.CSV :  Durango
    16 .  mexique/CSV/TR_VIVIENDA15.CSV :  México
    17 .  mexique/CSV/TR_VIVIENDA28.CSV :  Tamaulipas
    18 .  mexique/CSV/TR_PERSONA12.CSV :  Guerrero
    19 .  mexique/CSV/TR_VIVIENDA25.CSV :  Sinaloa
    20 .  mexique/CSV/TR_PERSONA16.CSV :  Michoacán de Ocampo
    21 .  mexique/CSV/TR_VIVIENDA21.CSV :  Puebla
    22 .  mexique/CSV/TR_VIVIENDA16.CSV :  Michoacán de Ocampo
    23 .  mexique/CSV/TR_VIVIENDA17.CSV :  Morelos
    24 .  mexique/CSV/TR_PERSONA25.CSV :  Sinaloa
    25 .  mexique/CSV/TR_PERSONA03.CSV :  Baja California Sur
    26 .  mexique/CSV/TR_VIVIENDA04.CSV :  Campeche
    27 .  mexique/CSV/TR_PERSONA31.CSV :  Yucatán
    28 .  mexique/CSV/TR_VIVIENDA26.CSV :  Sonora
    29 .  mexique/CSV/TR_VIVIENDA30.CSV :  Veracruz de Ignacio de la Llave
    30 .  mexique/CSV/TR_PERSONA29.CSV :  Tlaxcala
    31 .  mexique/CSV/TR_PERSONA26.CSV :  Sonora
    32 .  mexique/CSV/TR_VIVIENDA32.CSV :  Zacatecas
    33 .  mexique/CSV/TR_PERSONA04.CSV :  Campeche
    34 .  mexique/CSV/TR_PERSONA17.CSV :  Morelos
    35 .  mexique/CSV/TR_VIVIENDA14.CSV :  Jalisco
    36 .  mexique/CSV/TR_PERSONA24.CSV :  San Luis Potosí
    37 .  mexique/CSV/TR_VIVIENDA07.CSV :  Chiapas
    38 .  mexique/CSV/TR_PERSONA06.CSV :  Colima
    39 .  mexique/CSV/TR_VIVIENDA09.CSV :  Distrito Federal
    40 .  mexique/CSV/TR_PERSONA32.CSV :  Zacatecas
    41 .  mexique/CSV/TR_PERSONA08.CSV :  Chihuahua
    42 .  mexique/CSV/TR_VIVIENDA27.CSV :  Tabasco
    43 .  mexique/CSV/TR_PERSONA28.CSV :  Tamaulipas
    44 .  mexique/CSV/TR_PERSONA18.CSV :  Nayarit
    45 .  mexique/CSV/TR_VIVIENDA24.CSV :  San Luis Potosí
    46 .  mexique/CSV/TR_PERSONA01.CSV :  Aguascalientes
    47 .  mexique/CSV/TR_VIVIENDA13.CSV :  Hidalgo
    48 .  mexique/CSV/TR_PERSONA09.CSV :  Distrito Federal
    49 .  mexique/CSV/TR_VIVIENDA22.CSV :  Querétaro
    50 .  mexique/CSV/TR_VIVIENDA12.CSV :  Guerrero
    51 .  mexique/CSV/TR_PERSONA22.CSV :  Querétaro
    52 .  mexique/CSV/TR_VIVIENDA05.CSV :  Coahuila de Zaragoza
    53 .  mexique/CSV/TR_VIVIENDA06.CSV :  Colima
    54 .  mexique/CSV/TR_PERSONA15.CSV :  México
    55 .  mexique/CSV/TR_PERSONA14.CSV :  Jalisco
    56 .  mexique/CSV/TR_PERSONA30.CSV :  Veracruz de Ignacio de la Llave
    57 .  mexique/CSV/TR_VIVIENDA29.CSV :  Tlaxcala
    58 .  mexique/CSV/TR_VIVIENDA18.CSV :  Nayarit
    59 .  mexique/CSV/TR_VIVIENDA20.CSV :  Oaxaca
    60 .  mexique/CSV/TR_PERSONA13.CSV :  Hidalgo
    61 .  mexique/CSV/TR_VIVIENDA03.CSV :  Baja California Sur
    62 .  mexique/CSV/TR_PERSONA10.CSV :  Durango
    63 .  mexique/CSV/TR_PERSONA20.CSV :  Oaxaca
    64 .  mexique/CSV/TR_VIVIENDA11.CSV :  Guanajuato


## Séparation des fichiers VVIENDA et PERSONA


```python
persona_zm_mexico = [] # liste des fichiers PERSONA de la ZM de Mexico
vivienda_zm_mexico = [] # liste des fichiers VIVIENDA de la ZM de Mexico

import re
pattern = re.compile (r'TR_VIVIENDA.*')
for fichier in liste_zm_mexico:
    if pattern.search(fichier):
        vivienda_zm_mexico.append(fichier)
    else:
        persona_zm_mexico.append(fichier)
       
```


```python
for fichier in vivienda_zm_mexico :
    print(fichier)
    df = pd.read_csv(fichier, encoding = 'latin_1')
    df.shape
```

    mexique/CSV/TR_VIVIENDA15.CSV
    mexique/CSV/TR_VIVIENDA09.CSV


### Concaténation des fichiers


```python
# pour chaque fichier dans la liste, pandas ouvre le csv, le convertit en dataframe et concatène les fichiers
vivienda_mx= pd.concat([ pd.read_csv(df, encoding = 'latin_1') for df in  vivienda_zm_mexico])
vivienda_mx.shape
```




    (640686, 88)




```python
persona_mx= pd.concat([ pd.read_csv(df, encoding = 'latin_1') for df in  persona_zm_mexico])
persona_mx.shape
```




    (2475057, 86)



### fusion des bases VIVIENDA et PERSONA


```python
result = pd.merge(persona_mx, vivienda_mx, on='ID_VIV')
result.shape
```




    (2475057, 173)




```python
result.to_csv('persona_viviend_zm_mexico.csv', sep=',', encoding='utf-8')
```
